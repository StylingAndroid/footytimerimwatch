package com.stylingandroid.footytimer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends Activity {
	private static final String TAG = "Footy Timer";

	private TextView mTime;
	private TextView mPlayed;
	private TextView mCurrent;
	private TextView mStoppages;
	private TextView mGross;

	static final private Format sTimeFmt = new SimpleDateFormat("HH:mm");

	private long mStartTime = 0;
	private long mAccumulated = 0;
	private long mAllStoppages = 0;
	private long mCurrentStoppage = 0;

	private Handler mHandler;

	private GestureDetector mDetector;
	private GestureDetector.OnGestureListener mListener = new GestureDetector.SimpleOnGestureListener() {
		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			if (mStartTime == 0) {
				startTimer();
			} else if (mCurrentStoppage == 0) {
				pauseTimer();
			} else {
				resumeTimer();
			}
			return true;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			if (mStartTime > 0) {
				stopTimer();
			} else {
				clearTimer();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTime = (TextView) findViewById(R.id.time);
		mPlayed = (TextView) findViewById(R.id.played);
		mCurrent = (TextView) findViewById(R.id.current);
		mStoppages = (TextView) findViewById(R.id.stoppages);
		mGross = (TextView) findViewById(R.id.gross);
		mHandler = new Handler();
		mDetector = new GestureDetector(this, mListener);
	}

	private void updateTime() {
		mTime.setText(sTimeFmt.format(new Date()));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return mDetector.onTouchEvent(event);
	}

	private void startTimer() {
		Log.v(TAG, "start");
		mStartTime = System.currentTimeMillis();
		mCurrentStoppage = 0;
	}

	private void stopTimer() {
		Log.v(TAG, "stop");
		mAccumulated += System.currentTimeMillis() - mStartTime;
		mStartTime = 0;
	}

	private void pauseTimer() {
		Log.v(TAG, "pause");
		mCurrentStoppage = System.currentTimeMillis();
		update();
	}

	private void resumeTimer() {
		Log.v(TAG, "resume");
		mAllStoppages += System.currentTimeMillis() - mCurrentStoppage;
		mCurrentStoppage = 0;
		update();
	}

	private void clearTimer() {
		Log.v(TAG, "clear");
		mStartTime = 0;
		mAccumulated = 0;
		mCurrentStoppage = 0;
		mAllStoppages = 0;
		update();
	}

	private void update() {
		long now = System.currentTimeMillis();
		long elapsed = mAccumulated;
		if (mStartTime > 0) {
			elapsed += now - mStartTime;
		}
		if (elapsed > 99 * 60000) {
			elapsed = 99 * 60000;
			mStartTime = 0;
		}
		long allStoppages = mAllStoppages;
		if (mCurrentStoppage > 0) {
			allStoppages += now - mCurrentStoppage;
			mCurrent.setVisibility(View.VISIBLE);
			mCurrent.setText(formatTime(now - mCurrentStoppage, true));
			mStoppages.setVisibility(View.VISIBLE);
			mStoppages.setText(formatTime(allStoppages, true));
			mPlayed.setVisibility(View.VISIBLE);
		} else {
			mCurrent.setVisibility(View.INVISIBLE);
			if (allStoppages == 0) {
				mStoppages.setVisibility(View.INVISIBLE);
				mPlayed.setVisibility(View.INVISIBLE);
			} else {
				mStoppages.setVisibility(View.VISIBLE);
				mPlayed.setVisibility(View.VISIBLE);
				mStoppages.setText(formatTime(allStoppages, true));
			}
		}
		mPlayed.setText(formatTime(elapsed - allStoppages, true));
		mGross.setText(formatTime(elapsed, false));
		updateTime();
	}

	private String formatTime(long millis, boolean round) {
		long mins = millis / 60000;
		long secs = ((millis - (mins * 60000)) + (round ? 500 : 0)) / 1000;
		return getResources().getString(R.string.time, mins, secs);
	}
}
